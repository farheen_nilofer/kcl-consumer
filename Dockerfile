FROM maven:3.6.1-jdk-11-slim AS build
COPY pom.xml /tmp/
COPY src /tmp/src/
WORKDIR /tmp/
RUN mvn package
FROM openjdk:8u181-jdk-alpine3.8
COPY --from=build /tmp/target/kcl-consumer-1.0-SNAPSHOT-jar-with-dependencies.jar /kcl.jar
CMD java -jar kcl.jar