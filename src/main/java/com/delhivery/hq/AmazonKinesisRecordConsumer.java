package com.delhivery.hq;
import com.delhivery.hq.JavaThriftClient;
import com.delhivery.hq.dex.ReadResponse;
import com.delhivery.hq.dex.InData;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.thrift.TException;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.cloudwatch.CloudWatchAsyncClient;
import software.amazon.awssdk.services.dynamodb.DynamoDbAsyncClient;
import software.amazon.awssdk.services.kinesis.KinesisAsyncClient;
import software.amazon.kinesis.common.ConfigsBuilder;
import software.amazon.kinesis.common.InitialPositionInStream;
import software.amazon.kinesis.common.InitialPositionInStreamExtended;
import software.amazon.kinesis.common.KinesisClientUtil;
import software.amazon.kinesis.coordinator.Scheduler;
import software.amazon.kinesis.exceptions.InvalidStateException;
import software.amazon.kinesis.exceptions.ShutdownException;
import software.amazon.kinesis.lifecycle.events.*;
import software.amazon.kinesis.processor.ShardRecordProcessor;
import software.amazon.kinesis.processor.ShardRecordProcessorFactory;
import software.amazon.kinesis.retrieval.KinesisClientRecord;
import software.amazon.kinesis.retrieval.polling.PollingConfig;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.TimeUnit;

//import com.amazonaws.kinesis.deagg.RecordDeaggregator;

/**
 * This class will run a simple app that uses the KCL to read data and uses the AWS SDK to publish data.
 * Before running this program you must first create a Kinesis stream through the AWS console or AWS SDK.
 */
public class AmazonKinesisRecordConsumer {

    private final String streamName = System.getenv("KCL_STREAM_NAME");
    private final Region region = Region.of(ObjectUtils.firstNonNull(System.getenv("STREAM_REGION"), "us-east-2"));
    private final String applicationId = System.getenv("KCL_APP_ID");
    private final KinesisAsyncClient kinesisClient;

    /**
     * Constructor sets streamName and region. It also creates a KinesisClient object to send data to Kinesis.
     * This KinesisClient is used to send dummy data so that the consumer has something to read; it is also used
     * indirectly by the KCL to handle the consumption of the data.
     */
    public AmazonKinesisRecordConsumer() {
        this.kinesisClient = KinesisClientUtil.createKinesisAsyncClient(KinesisAsyncClient.builder().region(this.region));
        if(this.streamName == null || this.applicationId == null || this.kinesisClient == null){
            System.out.println("ERROR:Environment Variable KCL_STREAM_NAME, KCL_APP_ID or STREAM_REGION is/are missing");
            return;
        }
    }

    public void run() {

        /**
         * Sets up configuration for the KCL, including DynamoDB and CloudWatch dependencies. The final argument, a
         * ShardRecordProcessorFactory, is where the logic for record processing lives, and is located in a private
         * class below.
         */
        DynamoDbAsyncClient dynamoClient = DynamoDbAsyncClient.builder().region(region).build();
        CloudWatchAsyncClient cloudWatchClient = CloudWatchAsyncClient.builder().region(region).build();
        ConfigsBuilder configsBuilder = new ConfigsBuilder(streamName, streamName+"_"+applicationId, kinesisClient, dynamoClient, cloudWatchClient, UUID.randomUUID().toString(), new SampleRecordProcessorFactory());

        /**
         * The Scheduler (also called Worker in earlier versions of the KCL) is the entry point to the KCL. This
         * instance is configured with defaults provided by the ConfigsBuilder.
         */

        Scheduler scheduler = new Scheduler(
                configsBuilder.checkpointConfig(),
                configsBuilder.coordinatorConfig(),
                configsBuilder.leaseManagementConfig(),
                configsBuilder.lifecycleConfig(),
                configsBuilder.metricsConfig(),
                configsBuilder.processorConfig(),
                configsBuilder.retrievalConfig().retrievalSpecificConfig(new PollingConfig(streamName, kinesisClient)).initialPositionInStreamExtended(InitialPositionInStreamExtended.newInitialPosition(InitialPositionInStream.LATEST))
        );

        /**
         * Kickoff the Scheduler. Record processing of the stream of dummy data will continue indefinitely
         * until an exit is triggered.
         */
        Thread schedulerThread = new Thread(scheduler);
//        schedulerThread.setDaemon(true);
        schedulerThread.start();
        try {
            schedulerThread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("Will never ever be printed at all");
        /**
         * Allows termination of app by pressing Enter.
         */
        System.out.println("Press enter to shutdown");
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        try {
            reader.readLine();
        } catch (IOException ioex) {
            System.out.println("Caught exception while waiting for confirm. Shutting down. " + ioex.toString());

        }

        /**
         * Stops consuming data. Finishes processing the current batch of data already received from Kinesis
         * before shutting down.
         */
        Future<Boolean> gracefulShutdownFuture = scheduler.startGracefulShutdown();
        System.out.println("Waiting up to 20 seconds for shutdown to complete.");
        try {
            gracefulShutdownFuture.get(20, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            System.out.println("Interrupted while waiting for graceful shutdown. Continuing.");
        } catch (ExecutionException e) {
            System.out.println("Exception while executing graceful shutdown." + e.toString());
        } catch (TimeoutException e) {
            System.out.println("Timeout while waiting for shutdown.  Scheduler may not have exited.");
        }
        System.out.println("Completed, shutting down now.");
    }

    /**
     * Sends a single record of dummy data to Kinesis.
     */

    private static class SampleRecordProcessorFactory implements ShardRecordProcessorFactory {
        public ShardRecordProcessor shardRecordProcessor() {
            return new SampleRecordProcessor();
        }
    }

    /**
     * The implementation of the ShardRecordProcessor interface is where the heart of the record processing logic lives.
     * In this example all we do to 'process' is log info about the records.
     */
    private static class SampleRecordProcessor implements ShardRecordProcessor {

        private static final String SHARD_ID_MDC_KEY = "ShardId";


        private String shardId;

        /**
         * Invoked by the KCL before data records are delivered to the ShardRecordProcessor instance (via
         * processRecords). In this example we do nothing except some logging.
         *
         * @param initializationInput Provides information related to initialization.
         */
        public void initialize(InitializationInput initializationInput) {
            shardId = initializationInput.shardId();
        }

        /**
         * Handles record processing logic. The Amazon Kinesis Client Library will invoke this method to deliver
         * data records to the application. In this example we simply log our records.
         *
         * @param processRecordsInput Provides the records to be processed as well as information and capabilities
         *                            related to them (e.g. checkpointing).
         */
        public void processRecords(ProcessRecordsInput processRecordsInput) {
//            MDC.put(SHARD_ID_MDC_KEY, shardId);
            System.out.println("::processRecordsInput::");
            List<KinesisClientRecord> records = processRecordsInput.records();
            List<String> inputList = new ArrayList<String>();

            for(KinesisClientRecord rec: records){
                try {
                    System.out.println("::Rec Data::");
                    String test_str = Charset.forName("UTF-8").decode(rec.data()).toString();
                    System.out.println(test_str);
                    inputList.add(test_str);
//                    System.out.println("::inputList::Size: "+inputList.size());
//                    System.out.println("::inputList:: Data:"+inputList);
                } catch (Exception e) {
                    System.out.println("ERROR: "+e.getMessage());
                    e.printStackTrace();
                }
            }
            System.out.println("Data recieved from Kinesis. Data: "+inputList);
            float backoffRate = 0.002f;
            int sleepTime = 1;
            int currentSleepTime = 1000;
            ReadResponse response;
            List<InData> thriftPayload = JavaThriftClient.createBulkThriftPayload(inputList);
//            System.out.println("thriftPayload: "+thriftPayload);
            while(true){
                try{
//                    System.out.println(":::::Enterring loop:::::");
                    response = JavaThriftClient.perform(thriftPayload);
                    System.out.println("<Response>: "+response.toString());
                    if(response == null){
                        continue;
                    }
                    break;
                }
                catch (TException e){
                    try
                    {
                        Thread.sleep((currentSleepTime));
                        currentSleepTime += (int)(currentSleepTime * backoffRate);
                    }
                    catch(InterruptedException ex)
                    {
                        Thread.currentThread().interrupt();
                        System.out.println("Exception occurred while trying to sleep/ Error: " + ex.getMessage());
                    }

                }

            }
            //polling here
            if(response.success){
                String batchID = response.batchID;
                JavaThriftClient.poll(batchID);
            }
            else {
                System.out.println("Fail to process the task. Batch Id: " + response.batchID);
            }
            System.out.println("Event Processd!");
            try {
                processRecordsInput.checkpointer().checkpoint();
            } catch (InvalidStateException e) {
                e.printStackTrace();
            } catch (ShutdownException e) {
                e.printStackTrace();
            }
        }

        /** Called when the lease tied to this record processor has been lost. Once the lease has been lost,
         * the record processor can no longer checkpoint.
         *
         * @param leaseLostInput Provides access to functions and data related to the loss of the lease.
         */
        public void leaseLost(LeaseLostInput leaseLostInput) {
        }

        /**
         * Called when all data on this shard has been processed. Checkpointing must occur in the method for record
         * processing to be considered complete; an exception will be thrown otherwise.
         *
         * @param shardEndedInput Provides access to a checkpointer method for completing processing of the shard.
         */
        public void shardEnded(ShardEndedInput shardEndedInput) {
            try {
                System.out.println("Reached shard end checkpointing.");
                shardEndedInput.checkpointer().checkpoint();
            } catch (ShutdownException | InvalidStateException e) {
                System.out.println("Exception while checkpointing at shard end. Giving up."+ e.toString());
            }
        }

        /**
         * Invoked when Scheduler has been requested to shut down (i.e. we decide to stop running the app by pressing
         * Enter). Checkpoints and logs the data a final time.
         *
         * @param shutdownRequestedInput Provides access to a checkpointer, allowing a record processor to checkpoint
         *                               before the shutdown is completed.
         */
        public void shutdownRequested(ShutdownRequestedInput shutdownRequestedInput) {
            try {
                System.out.println("Scheduler is shutting down, checkpointing.");
                shutdownRequestedInput.checkpointer().checkpoint();
            } catch (ShutdownException | InvalidStateException e) {
                System.out.println("Exception while checkpointing at requested shutdown. Giving up."+e.toString());
            }
        }
    }
}