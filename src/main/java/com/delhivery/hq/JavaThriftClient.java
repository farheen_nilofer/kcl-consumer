package com.delhivery.hq;
import com.delhivery.hq.dex.DexCore;
import com.delhivery.hq.dex.DexMap;
import com.delhivery.hq.dex.InData;
import com.delhivery.hq.dex.ReadResponse;
import org.apache.thrift.TException;
import org.apache.thrift.protocol.TBinaryProtocol;
import org.apache.thrift.protocol.TProtocol;
import org.apache.thrift.transport.TSSLTransportFactory;
import org.apache.thrift.transport.TSSLTransportFactory.TSSLTransportParameters;
import org.apache.thrift.transport.TSocket;
import org.apache.thrift.transport.TTransport;

import org.apache.thrift.transport.TTransportException;
import org.json.*;
import java.util.ArrayList;
import java.util.List;

public class JavaThriftClient {
    public static void main(String [] args) {

        if (args.length != 1) {
            System.out.println("Please enter 'simple' or 'secure'");
            System.exit(0);
        }

        try {
            TTransport transport;
            if (args[0].contains("simple")) {
                transport = new TSocket("localhost", 9091);
                transport.open();
            }
            else {
                TSSLTransportParameters params = new TSSLTransportParameters();
                params.setTrustStore("../../lib/java/test/.truststore", "thrift", "SunX509", "JKS");
                /*
                 * Get a client transport instead of a server transport. The connection is opened on
                 * invocation of the factory method, no need to specifically call open()
                 */
                transport = TSSLTransportFactory.getClientSocket("localhost", 9091, 0, params);
            }

            TProtocol protocol = new  TBinaryProtocol(transport);
            DexCore.Client client = new DexCore.Client(protocol);

//            perform(client);

            transport.close();
        } catch (TException x) {
            x.printStackTrace();
        }
    }


    public static ReadResponse perform(List<InData> data) throws TException
    {
        TTransport transport;
        transport = new TSocket("localhost", 9090);
        transport.open();
        TProtocol protocol = new  TBinaryProtocol(transport);
        DexCore.Client client = new DexCore.Client(protocol);
        ReadResponse resp2 = null;
        try {
            resp2 = client.readData(data);
        }
        catch (Exception err){
            System.out.println("Error in sending data over thrif. Error: "+err.getMessage());
        }
        System.out.println(resp2.toString());
        transport.close();
        return resp2;
    }

    public static void poll(String batchId){
        boolean isSuccess = false;
        int sleepTime = 20;
        while (!isSuccess){
            try{
                TTransport transport = new TSocket("localhost", 9090);
                transport.open();
                TProtocol protocol = new  TBinaryProtocol(transport);
                DexCore.Client client = new DexCore.Client(protocol);
                isSuccess = client.pollBatchStatus(batchId);
                System.out.println("Polling response: "+ isSuccess);
                transport.close();
            }
            catch (TException ex){
                isSuccess = false;
                try{
                    Thread.sleep(sleepTime);
                }catch(InterruptedException exxx)
                {
                    Thread.currentThread().interrupt();
                    System.out.println("Exception occurred while trying to sleep/ Error: " + exxx.getMessage());
                }
            }
        }
    }

    public static InData createThriftPayload(String record){

        JSONObject obj = new JSONObject(record);
        InData indata = new InData();
        indata.Data = obj.getString("Data");
        List<DexMap> dexMapList = new ArrayList<DexMap>();
        JSONArray dexMapJsonArr = obj.getJSONArray("Config");

        int len = dexMapJsonArr.length();

        for(int i = 0; i<len; i++){
            DexMap dex_map = new DexMap();
            JSONObject jObj =  dexMapJsonArr.getJSONObject(i);
            dex_map.key = jObj.getString("key");
            dex_map.val = jObj.getString("val");
            dexMapList.add( dex_map);
        }

        indata.Config = dexMapList;
        indata.itemID = obj.getString("itemID");
        indata.OrderKey = obj.getString("OrderKey");
        return indata;
    }



    public static List<InData> createBulkThriftPayload(List<String> records) {
        List<InData> thrift_payload = new ArrayList<InData>();
//        System.out.println("UnFormatted thrift payload: "+records);
        for (String rec : records
        ) {
            System.out.println("rec: "+rec);
        JSONObject obj = new JSONObject(rec);
            InData indata = new InData();
            if (!(obj.has("Data") && obj.has("Config"))){
                continue;
            }
            indata.Data = obj.getString("Data");
            List<DexMap> dexMapList = new ArrayList<DexMap>();
            JSONArray dexMapJsonArr = obj.getJSONArray("Config");

            for (int i = 0; i < dexMapJsonArr.length(); i++) {
                try {
                    DexMap dex_map = new DexMap();
                    JSONObject jObj = dexMapJsonArr.getJSONObject(i);
                    dex_map.key = jObj.getString("key");
                    dex_map.val = jObj.getString("val");
                    dexMapList.add(dex_map);
                } catch (JSONException e) {
                }
            }
            indata.Config = dexMapList;
            try {
                indata.itemID = obj.getString("itemID");
            } catch (JSONException e) {
            }
            try {
                indata.OrderKey = obj.getString("OrderKey");
            } catch (JSONException e) {
            }
//            System.out.println("indata: "+indata);
            thrift_payload.add(indata);

        }
        System.out.println("Formatted thrift payload: "+thrift_payload.toString());
        return thrift_payload;
    }
}