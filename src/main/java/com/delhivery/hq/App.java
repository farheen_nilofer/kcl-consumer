package com.delhivery.hq;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main(String... args) {
//        System.out.println("Arguments passed: "+args[0]+" "+args[1]);
        System.out.println("Environment variables needed. Starting KCL...");
//        if (args.length < 1) {
////            log.error("At a minimum, the stream name is required as the first argument. The Region may be specified as the second argument.");
//            System.exit(1);
//        }
//
//        String streamName = args[0];
//        String region = null;
//        if (args.length > 1) {
//            region = args[1];
//        }

        new AmazonKinesisRecordConsumer().run();
    }
}