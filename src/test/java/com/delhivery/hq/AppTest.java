package com.delhivery.hq;
import com.delhivery.hq.JavaThriftClient;
import com.delhivery.hq.dex.InData;

import static org.junit.Assert.assertTrue;

import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * Unit test for simple App.
 */
public class AppTest 
{
    /**
     * Rigorous Test :-)
     */
    @Test
    public void shouldAnswerWithTrue()
    {
        List<String> inputList = new ArrayList<String>();
        inputList.add("{\"itemID\": \"itemID1\", \"Config\": [{\"val\": \"val1\", \"key\": \"key1\"}, {\"val\": \"val2\", \"key\": \"key2\"}], \"Data\": \"This is a test data\", \"OrderKey\": \"OrderKey1\"}");
        List<InData> thrift_payload= JavaThriftClient.createBulkThriftPayload(inputList);
        Assert.assertEquals(thrift_payload.get(0).Data,"This is a test data");
        Assert.assertEquals(thrift_payload.get(0).itemID,"itemID1");
        Assert.assertEquals(thrift_payload.get(0).OrderKey,"OrderKey1");
        List<String> inputList2 = new ArrayList<String>();
        inputList2.add("{\"itemID\": \"\", \"Config\": [{\"val\": \"val1\", \"key\": \"key1\"}, {\"val\": \"val2\", \"key\": \"key2\"}], \"Data\": \"This is a test data\", \"OrderKey\": \"\"}");
        thrift_payload= JavaThriftClient.createBulkThriftPayload(inputList2);
        Assert.assertNotEquals(thrift_payload.get(0).OrderKey,"OrderKey1");
        Assert.assertNotEquals(thrift_payload.get(0).itemID,"itemID1");
        List<String> inputList3 = new ArrayList<String>();
        inputList3.add("{ \"Config\": [{\"val\": \"val1\", \"key\": \"key1\"}, {\"val\": \"val2\", \"key\": \"key2\"}], \"Data\": \"This is a test data\", \"OrderKey\": \"\"}");
        thrift_payload= JavaThriftClient.createBulkThriftPayload(inputList3);
        Assert.assertNotEquals(thrift_payload.get(0).OrderKey,"OrderKey1");
        Assert.assertNotEquals(thrift_payload.get(0).itemID,"itemID1");
    }
}
